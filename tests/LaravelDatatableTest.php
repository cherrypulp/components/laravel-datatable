<?php

namespace Cherrypulp\LaravelDatatable\Tests;

use Cherrypulp\LaravelDatatable\Facades\LaravelDatatable;
use Cherrypulp\LaravelDatatable\ServiceProvider;
use Orchestra\Testbench\TestCase;

class LaravelDatatableTest extends TestCase
{
    protected function getPackageProviders($app)
    {
        return [ServiceProvider::class];
    }

    protected function getPackageAliases($app)
    {
        return [
            'laravel-datatable' => LaravelDatatable::class,
        ];
    }

    public function testExample()
    {
        $this->assertEquals(1, 1);
    }
}
