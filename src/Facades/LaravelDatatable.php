<?php

namespace Cherrypulp\LaravelDatatable\Facades;

use Illuminate\Support\Facades\Facade;

class LaravelDatatable extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'laravel-datatable';
    }
}
