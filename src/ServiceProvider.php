<?php

namespace Cherrypulp\LaravelDatatable;

class ServiceProvider extends \Illuminate\Support\ServiceProvider
{
    const CONFIG_PATH = __DIR__ . '/../config/laravel-datatable.php';

    public function boot()
    {
        $this->publishes([
            self::CONFIG_PATH => config_path('laravel-datatable.php'),
        ], 'config');
    }

    public function register()
    {
        $this->mergeConfigFrom(
            self::CONFIG_PATH,
            'laravel-datatable'
        );

        $this->app->bind('laravel-datatable', function () {
            return new LaravelDatatable();
        });
    }
}
