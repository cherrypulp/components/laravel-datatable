# Laravel Datatable

This is a fork of Laravel Datatables (https://github.com/yajra/laravel-datatables)

## Installation

Install via composer

```bash
composer require cherrypulp/laravel-datatable
```

### Register Service Provider

**Note! This and next step are optional if you use laravel>=5.5 with package
auto discovery feature.**

Add service provider to `config/app.php` in `providers` section
```php
Cherrypulp\LaravelDatatable\ServiceProvider::class,
```

### Register Facade

Register package facade in `config/app.php` in `aliases` section
```php
Cherrypulp\LaravelDatatable\Facades\LaravelDatatable::class,
```

### Publish Configuration File

```bash
php artisan vendor:publish --provider="Cherrypulp\LaravelDatatable\ServiceProvider" --tag="config"
```

## Usage

For now it's just an adapter helper to load Laravel-Datatables and Laravel-Datatables-Oracle.

So for the documentation please check at : 

See : https://laravel-datatables-docs.netlify.com/



## Security

If you discover any security related issues, please email 
instead of using the issue tracker.

## Credits
- [Daniel Sum](https://github.com/danielsum)
- [Arjay Angeles](https://github.com/yajra)
- [bllim/laravel4-datatables-package](https://github.com/bllim/laravel4-datatables-package)
- [All Contributors](https://github.com/yajra/laravel-datatables/graphs/contributors)

This package is bootstrapped with the help of
[blok/laravel-package-generator](https://github.com/blok/laravel-package-generator).
